/**
* @jsx React.DOM
*/

'use strict';
var $ = require('jquery');
var _ = require('underscore');
var React = require('react');

var Fluxxor = require("fluxxor"),
FluxMixin = Fluxxor.FluxMixin(React),
StoreWatchMixin = Fluxxor.StoreWatchMixin
Router = require('react-router'),
Navigation = Router.Navigation,

ContentItem = require('./../mixins/ContentItem.js'),

AddContentItem = require('./AddContentItem.jsx'),
ContentList = require('./BaseContentList.jsx'),
EditorWrapper = require('./EditorWrapper.jsx'),
TagItem = require('./TagItem.jsx'),
CommentItem = require('./CommentContent.jsx');


var ReadBlogItem = React.createClass({

	getInitialState() {
		return {
			comments: (this.props.obj && this.props.obj.comments || [])
		}
	},

	mixins: [Navigation, ContentItem.ReadMode, FluxMixin, StoreWatchMixin("BlogStore")],

	getStateFromFlux: function() {
		return {}
	},

	onAddComment(comment) {

		var self = this;

		var date = new Date(),
		year = date.getFullYear(),
		month = (date.getMonth() + 1 >= 10) ? date.getMonth() + 1 : "0" + (date.getMonth() + 1),
		day = date.getDate(),
		hours = date.getHours(),
		minutes = date.getMinutes();

		comment.date = day + "." + month + "." + year + " " + hours + ":" + minutes;
		comment.article = this.props.obj.id;
		this.state.comments.push(comment);

		this.setState({
			comments: this.state.comments
		});

		this.getFlux().actions.content.addComment(comment);

		var article = this.props.obj;
		article.documents = [this.state.imageId || article.image];
		article.categoryId = this.props.obj.category.id;
		this.getFlux().actions.content.editArticle(article.id, article);
	},

	render: function () {

		var title = this.props.obj.title,
		bodyText = this.props.obj.body,
		date = this.props.obj.date.split(" ")[0],
		category = this.props.obj.category.name,
		adminButtons = this.renderButtons();

		var tags = 
			<ContentList
				list={this.props.obj.tags}  
				isAdmin={false} 
				element={TagItem} />;

		var comments = 
			<ContentList
				list={this.state.comments}
				isAdmin={true}
				onAdd = {this.onAddComment}
				element={CommentItem} />;

		return (
			<div className="blog-item clearfix" key={this.props.obj.id}>
				<div className="row">
					<div className="blog-item-title-section form-group col-xs-12">
						<p className="blog-item-title">{title}</p>
					</div>
				</div>
				<div className="row">
					<div className="blog-item-body <col-xs-12></col-xs-12>" dangerouslySetInnerHTML={{__html: bodyText}} />
				</div>
				<div className="row">
					<div className="blog-item-date form-group col-xs-2">
						{this.props.obj.date.split(" ")[0]}
					</div>
					<div className="blog-item-category form-group col-xs-2">
						{category}
					</div>
					<div className="blog-item-tags-section form-group col-xs-8">
						{tags}
					</div>
				</div>

				{adminButtons}

				<div className="blog-comments-section form-group col-xs-12">
					<p className="blog-comments-section-title">{"Comments (" + this.props.obj.comments.length + ")"}</p>
					{comments}
				</div>

			</div>
		);
	}
});

var WriteBlogItem = React.createClass({

	getInitialState() {
		return {
			tags: (this.props.obj && this.props.obj.tags || []),
			categoryId: (this.props.obj && this.props.obj.category.id),
			comments: (this.props.obj && this.props.obj.comments || []),
			removedComments: [],   // TODO: find another solution!
			removedTags: []
		};
	},

	mixins: [ContentItem.WriteMode, FluxMixin, StoreWatchMixin("BlogStore")],

	getDefaultProps: function () {
		return {
			basePath: '/uploads/',
		}
	},

	getStateFromFlux: function() {
		var blogStore = this.getFlux().store("BlogStore");
		return {
			blogCategories: blogStore.blogCategories
		}
	},

	addItem: function (event) {
		event.preventDefault();

		this.props.onAdd({
			author: "admin",
			title: this.refs.title.getDOMNode().value,
			body: this.state.body,
			categoryId: this.state.categoryId || this.state.blogCategories[0].id,
			documents: [this.state.imageId],
			tags: this.state.tags || []
		});
		  
		this.props.cancelHandler();

	},

	updateItem: function (event) {
		event.preventDefault();

		var self = this;

		var obj = this.props.obj;
		obj.author = "admin"
		obj.title = this.refs.title.getDOMNode().value;
		obj.body = this.state.body || this.props.obj.body;
		obj.categoryId = this.state.categoryId;
		obj.documents = [this.state.imageId || obj.image];
		obj.tags = this.state.tags.concat(this.state.removedTags);
		obj.comments = this.state.comments;

		this.state.removedComments.forEach(function(commentId) {
			self.getFlux().actions.content.removeComment(commentId);
		});

		this.props.onEdit(this.props.obj.id, obj);
		this.props.cancelHandler();

	},

	openMediaForEditor: function () {
		this.setState({
			openOverlayForEditor: true
		});
	},

	handlePickerForEditor: function (document) {
		var path = this.props.basePath + document.categoryName + '/' + document.name;
		this.refs.body.refs.injectinput.getDOMNode().value = path;
		var a = this.refs.body.inject('image');
		this.handleOnOverlayClose();
	},

	onAddTag (obj) {

		var tags = [];
		if (!this.state.tags.push) {
			for(var prop in this.state.tags) {
				tags.push({
					"id": this.state.tags[prop].id,
					"title": this.state.tags[prop].title
				});
			}
			this.state.tags = tags;
		}

		this.state.tags.push(obj);

		this.setState({
			tags: this.state.tags
		});
	},

	onEditTag (obj) {
		var tags = _.map(this.state.tags, function (tag) {
			if(tag.id === obj.id) {
				tag = obj;
				tag.key = "update";
			}
			return tag;
		});
		  
		this.setState({
			tags: tags
		});
	},

	onRemoveTag (id) {
		var self = this;

		this.state.tags.forEach(function(tag) {
			if(tag.id === id) {
				tag.key = "delete";
				self.state.removedTags.push(tag);
			}
		});

		this.setState({
			tags: _.without(this.state.tags, _.findWhere(this.state.tags, {id: id}))
		});
	},

	onChangeCategory () {
		var element = this.refs.changeBlogCategory.getDOMNode();
		this.setState({
			categoryId: element.value
		});
	},

	renderCategoryList () {
		var options = _.map(this.state.blogCategories, function(blogCategory) {
			return <option value = {blogCategory.id}>{blogCategory.name}</option>;
		});

		return (
		<div className="category-list col-xs-12">
			<div className="select-category-label col-xs-4">
				<h4>Select Blog Category: </h4>
			</div>
			<div className="select-category col-xs-8">
				<select ref="changeBlogCategory" value={this.state.categoryId} onChange={this.onChangeCategory}>
					{options}
				</select>
			</div>
		</div>
		);
	},

	onRemoveComment(id) {

		this.state.removedComments.push(id);

		this.setState({
			comments: _.without(this.state.comments, _.findWhere(this.state.comments, {id: id})),
			removedComments: this.state.removedComments
		});
	},

	render: function () {

		var title, body, blogCategories;

		var buttons = this.renderButtons();

		var tags = 
			<ContentList
				list={this.state.tags}  
				isAdmin={true} 
				onAdd={this.onAddTag}
				onEdit={this.onEditTag}
				onRemove={this.onRemoveTag}
				element={TagItem} />;

		var comments = 
			<ContentList
				list={this.state.comments}
				isAdmin={true}
				onRemove={this.onRemoveComment}
				element={CommentItem} />;

		if(!this.props.isAddMode) {
			title = this.props.obj.title;
			body = this.state.body || this.props.obj.body;;
		}

		if (this.state.blogCategories) {
			blogCategories = this.renderCategoryList();
		}

		return (
			<div className="blog-edit-item clearfix">
				<div className="row">
					<div className="blog-item-title-section form-group col-xs-12">
						<input type="text" className="form-control" placeholder="Title" defaultValue={title} ref="title" />
					</div>
				</div>
				<div className="row">
					<div className="blog-item-body-section form-group col-xs-12">
						<EditorWrapper content={body} onChange={this.onBodyChange} />
					</div>
				</div>
				<div className="row">
					{blogCategories}
				</div>
				<div className="row">
					<div className="blog-tags-section form-group col-xs-12">
						<h4 className="blog-tags-section-title">Tags</h4>
						{tags}
					</div>
				</div>
				<div className="row">
					<div className="blog-comments-section form-group col-xs-12">
						<h4 className="blog-comments-section-title">{"Comments (" + this.state.comments.length + ")"}</h4>
						{comments}
					</div>
				</div>
				<div className="row">
					<div className="edit-buttons form-group col-sm-12">
						<div className="btn-group"> 
							{buttons}
						</div>
					</div>
				</div>
			</div>
		);
	}
});


var BlogContent = React.createClass({

	mixins: [ContentItem.BaseContent],

	render: function () {
		var blogMode = this.renderMode(ReadBlogItem, WriteBlogItem, AddContentItem);

		return (
			<div className="">{blogMode}</div>
		);
	}
});

module.exports = BlogContent;