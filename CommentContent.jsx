/**
 * @jsx React.DOM
 */

'use strict';
var $ = require('jquery');
var _ = require('underscore');
var React = require('react');

var Router = require('react-router'),
Navigation = Router.Navigation;

var ContentItem = require('./../mixins/ContentItem.js');
var AddContentItem = require('./AddContentItem.jsx');
var ContentList = require('./BaseContentList.jsx');

var EditorWrapper = require('./EditorWrapper.jsx');

var ReadCommentItem = React.createClass({

	mixins: [ContentItem.ReadMode],

	render: function () {

		var author = this.props.obj.author,
		bodyText = this.props.obj.body,
		date = this.props.obj.date,
		adminButtons = this.renderButtons(),
		dateString = date.split(" ")[0] + " at " + date.split(" ")[1];
		
		return (
			<div className="comment-item clearfix" key={this.props.obj.id}>

				<div className="row">
					<div className="comment-item-author col-xs-5"><span className="comment-item-author-label">Author: </span>{author}</div>
					<div className="comment-item-date col-xs-5"><span className="comment-item-date-label">Added </span>{dateString}</div>
					{adminButtons}
				</div>
				<div className="row">
					<div className="comment-item-body">{bodyText}</div>
				</div>

			</div>
		);
	}
});

var WriteCommentItem = React.createClass ({

	mixins: [ContentItem.WriteMode],

	addItem: function (event) {
		event.preventDefault();
		
		this.props.onAdd({
			author: this.refs.author.getDOMNode().value,
			body: this.refs.body.getDOMNode().value,
		});
		
		this.props.cancelHandler();
	},

	render: function() {

		var author, body;

		if(!this.props.isAddMode) {
			author = this.props.obj.author;
			body = this.props.obj.body;
		}

		var buttons = this.renderButtons();

		return (
			<div className="comment-edit-item clearfix">
				<div className="list-group-item row">
					<div className="row">
						<div className="comment-item-title-section form-group col-xs-12">
						 <input type="text" className="form-control" placeholder="Author" defaultValue={author} ref="author" />
						</div>
					</div>
					<div className="row">
						<div className="comment-item-body-section form-group col-xs-12">
							<textarea className="form-control" placeholder="Comment..." defaultValue={body} ref="body"></textarea>
						</div>
					</div>
					<div className="row">
						<div className="edit-buttons form-group col-sm-12">
							<div className="btn-group"> 
								{buttons}
							</div>
						</div>	 
					</div>
				</div>
			</div>
		);
	}
});

var CommentContent = React.createClass({

	mixins: [ContentItem.BaseContent],

	render: function () {
		return (
			<div className="">{this.renderMode(ReadCommentItem, WriteCommentItem, AddContentItem)}</div>
		);
	}
});

module.exports = CommentContent;