/**
* @jsx React.DOM
*/

'use strict';
var $ = require('jquery');
var _ = require('underscore');
var React = require('react');

var ContentItem = require('./../mixins/ContentItem.js');

var AdminMedia = require('./Admin/Media.jsx');
var AddContentItem = require('./AddContentItem.jsx');
var Modal = require('./Modal.jsx');


var ReadBlogCategoryItem = React.createClass({

	mixins: [ContentItem.ReadMode],

	render: function() {

		var adminButtons = this.renderButtons();

		return (
			<div className="blog-category-item clearfix">
				<div className="row">
					<div className="blog-category-item-name-section form-group col-xs-6">
						<span className="blog-category-item-name" onClick={this.props.clickHandler}>{this.props.obj.name}</span>
					</div>

					{adminButtons}

				</div>
			</div>
		);
	}
});


var WriteBlogCategoryItem = React.createClass({

	mixins: [ContentItem.WriteMode],

	addItem: function (event) {
		event.preventDefault();
		this.props.onAdd({name: this.refs.name.getDOMNode().value});
		this.props.cancelHandler();
	},

	updateItem: function (event) {
		event.preventDefault();

		var obj = this.props.obj;
		obj.name = this.refs.name.getDOMNode().value;
		this.props.onEdit(this.props.obj.id, obj);

		this.props.cancelHandler();
	},

	render: function () {
		var name, body;

		var buttons = this.renderButtons();

		if(!this.props.isAddMode) {
			name = this.props.obj.name;
		}

		return (
			<div className="blog-category-item clearfix">
				<div className="list-group-item row">	  
					<div className="blog-category-content form-group col-xs-12">
						<div className="row">
							<div className="blog-category-item-name-section form-group col-xs-12">
								<input type="text" className="form-control" placeholder="Name" defaultValue={name} ref="name" />
							</div>
						</div>
						<div className="row">
							<div className="edit-buttons form-group col-sm-12">
								<div className="btn-group"> 
									{buttons}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});

var BlogCategoryContent = React.createClass({

	mixins: [ContentItem.BaseContent],

	render: function () {
		var contentMode = this.renderMode(ReadBlogCategoryItem, WriteBlogCategoryItem, AddContentItem);

		return (
			<div className="blogCategoryItem">{contentMode}</div>
		);
	}
});


module.exports = BlogCategoryContent;