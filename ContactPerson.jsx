/**
 * @jsx React.DOM
 */

'use strict';
var $ = require('jquery');

var React = require('react');

var AdminMedia = require('./Admin/Media.jsx');
var ImageItem = require('./CategoryItem.jsx');
var Modal = require('./Modal.jsx');


var ReadContactPerson = React.createClass({

	removeElement: function(event) {
		event.preventDefault();
		this.props.onRemove(this.props.obj.id);
	},

	renderImage: function(document) {
		var basePath = "/uploads/";

		return (document && document.category && document.documentName) ?
			<img src={basePath + document.category + "/" + document.documentName} alt={document.documentName} width="100%" /> :
			null;
	},

	render: function () {

		var adminButtons = this.props.isAdmin ?
			(<div className="form-group clearfix">
				<div className="btn-group">
					<button type="submit" className="btn btn-primary" onClick={this.props.editHandler}>Edit</button>
					<button type="submit" className="btn btn-default" onClick={this.removeElement}>Remove</button>
				</div>
			</div>) :
			{};

		var image = this.renderImage(this.props.obj.document);

		return (
			<div className="list-group-item">
				
				<h3 className="contact-person-title">{this.props.obj.title}</h3>
				
				<div className="row">
					<div className="contact-person-img col-sm-4">{image}</div>
					<div className="col-sm-8 contact-person-body">{this.props.obj.body}</div>
				</div>
				
				<div className="contacts-section">
					<label>Contacts:</label>
					<div className="contact-person-email">{"Email: " + this.props.obj.email}</div>
					<div className="contact-person-email">{"Phone: " + this.props.obj.phone}</div>
					<div className="contact-person-email">{"Address: " + this.props.obj.address}</div>
				</div>

				{adminButtons}

			</div>
		);
	}
});


var EditContactPerson = React.createClass({

	getInitialState: function() {
		return {
			openOverlay: null,
			documentId: null
		};
	},

	updateElement: function (event) {
		event.preventDefault();

		var obj = this.props.obj;
		obj.title = this.refs.title.getDOMNode().value;
		obj.body = this.refs.body.getDOMNode().value;
		obj.document = +this.state.documentId || this.props.obj.document.id;
		obj.email = this.refs.email.getDOMNode().value;
		obj.phone = this.refs.phone.getDOMNode().value;
		obj.address = this.refs.address.getDOMNode().value;
		obj.identifiedWith = obj.identifiedWith.id;
		
		this.props.onEdit(obj.id, obj);
		this.props.cancelHandler();
	},

	addElement: function (event) {
		event.preventDefault();

		var obj = {
			title: this.refs.title.getDOMNode().value,
			body: this.refs.body.getDOMNode().value,
			document: +this.state.documentId,
			email: this.refs.email.getDOMNode().value,
			phone: this.refs.phone.getDOMNode().value,
			address: this.refs.address.getDOMNode().value,
			identifiedWith: this.props.category
		}
		
		this.props.onAdd(obj);
		this.props.closeAddItem();
	},

	openMedia: function () {
		this.setState({
			openOverlay: true
		});
	},

	handleOnOverlayClose: function () {
		this.setState({
			openOverlay: false
		});
	},

	handlePicker: function (document) {
		this.setState({
			openOverlay: false,
			documentId: document.id,
			documentName: document.name,
			categoryName: document.categoryName
		});
	},

	renderImage: function() {
		var basePath = "/uploads/";
		var document = (this.props.obj) ? this.props.obj.document : null;

		return (this.state.documentId || (document && document.category && document.documentName)) ?
			<img src={basePath + (this.state.categoryName || document.category) + "/" + (this.state.documentName || document.documentName)} width="100%"/> :
			null;
	},

	render: function () {
		var title, body, email, phone, address,
		image = this.renderImage();

		var openOverlay = this.state.openOverlay? 
			<Modal handleOnClose={this.handleOnOverlayClose} content={<AdminMedia handlePicker={this.handlePicker} />} flux={flux} />:
			{};

		var buttons = !this.props.addMode ?
			(<div className="btn-group">
				<button type="submit" className="btn btn-primary" onClick={this.updateElement}>Save</button>
				<button type="submit" className="btn btn-default" onClick={this.props.cancelHandler}>Cancel</button>
			</div>) :
			(<div className="btn-group">
				<button type="submit" className="btn btn-primary" onClick={this.addElement}>Add</button>
				<button type="submit" className="btn btn-default" onClick={this.props.closeAddItem}>Cancel</button>
			</div>);
		
		if (!this.props.addMode) {
			title = this.props.obj.title;
			body = this.props.obj.body;
			email = this.props.obj.email;
			phone = this.props.obj.phone;
			address = this.props.obj.address;
		}

		return (
			<div className="list-group-item">
				<div className="form-group">
					<input type="text" className="form-control" placeholder="Name" defaultValue={title} ref="title" />
				</div>
				<div className="form-group clearfix">
					<div className="col-sm-6">
						{image}
					</div>
					<div className="col-sm-6"> 
						<button type="submit" className="btn btn-default" onClick={this.openMedia}>Change Image</button>  
					</div>
					{openOverlay}
				</div>
				<div className="form-group">
					<textarea className="form-control" rows="3" placeholder="Body" defaultValue={body} ref="body"></textarea>
				</div>
				<div className="form-group">
					<input type="text" className="form-control" placeholder="Email" defaultValue={email} ref="email" />
				</div>
				<div className="form-group">
					<input type="text" className="form-control" placeholder="Phone" defaultValue={phone} ref="phone" />
				</div>
				<div className="form-group">
					<input type="text" className="form-control" placeholder="Address" defaultValue={address} ref="address" />
				</div>
				<div className="form-group">
					{buttons}
				</div>
			</div>
		);
	}
});


var NewContactPerson = React.createClass({
	  
	getInitialState: function() {
		return {
			inActive: true
		};
	},

	activateMode: function (event) {
		event.preventDefault();

		this.setState({
			inActive: this.state.inActive ? false : true
		});      
	},

	closeAddItem: function () {
		this.setState({
			inActive: this.state.inActive ? false : true
		});
	},

	render: function(){
		var mode = this.state.inActive ?
			<div className="form-group">
				<button type="submit" className="btn btn-primary" onClick={this.activateMode}>Add</button>
			</div>:
			<EditContactPerson  
				addMode={true} 
				onAdd={this.props.onAdd}
				closeAddItem={this.closeAddItem} 
				category={this.props.category} />;

		return (
			<div className="row">
				{mode}
			</div>
		)
	}
});


var ContactPerson = React.createClass({

	getInitialState: function() {
		return {
		isReadMode: true
		};
	},

	activateReadMode: function () {
		this.setState({
			isReadMode: true
		});
	},

	editElement: function () {
		this.setState({
			isReadMode: false
		});
	},

	render: function () {
		var mode;

		if (!this.props.obj) {
			mode = 
				<NewContactPerson 
					onAdd={this.props.onAdd} 
					cancelHandler={this.activateReadMode}
					category={this.props.category} />

		} else if (this.props.isAdmin && !this.state.isReadMode) {
			mode = 
				<EditContactPerson 
					obj={this.props.obj}
					cancelHandler={this.activateReadMode} 
					onEdit={this.props.onEdit} />

		} else {
			mode = 
				<ReadContactPerson
					isAdmin={this.props.isAdmin} 
					editHandler={this.editElement}
					onRemove={this.props.onRemove}
					obj={this.props.obj} />
		}

		return (
			<div>
				{mode}
			</div>
		);
	}
});

module.exports = ContactPerson;