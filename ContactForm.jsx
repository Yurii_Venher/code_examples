/**
 * @jsx React.DOM
 */

'use strict';

var $ = require('jquery');

var React = require('react'),
Modal = require('react-bootstrap').Modal;

var ContactForm = React.createClass({

	getInitialState: function() {
		return {
			sendStatus: null,
			formLoading: false,
			errorMessage: null
		};
	},

	sendMessage: function(event) {

		var self = this;

		var name = this.refs.name.getDOMNode().value,
		email = this.refs.email.getDOMNode().value,
		message = this.refs.message.getDOMNode().value;

		event.preventDefault();

		if(this.state.formLoading) return;

		if (!name || !email || !message) {
			this.setState({
				errorMessage: "All fields should be filled."
			});
			return;
		}

		var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
		if (!re.test(email)) {
			this.setState({
				errorMessage: "Email isn't valid"
			});
			return;
		}
   
		this.setState({
			formLoading: true,
			errorMessage: null
		});

		$.ajax({
			dataType: "json",
			contentType: "application/json",
			type: "POST",
			data: JSON.stringify({
				name: name,
				email: email,
				textarea: message
			}),
			url: '/api/contact/form',
			success: function(data) {
				self.setState({
					errorMessage: null,
					sendStatus: "Thank you. The message has been sent successfully",
					formLoading: false
				});
			},
			error: function(data) {
				self.setState({
					formLoading: false,
					errorMessage: "Something wrong happened. Try again."
				});
				$(".contact-form").reset();
			} 
		});
	},

	render: function() {

		var contactForm, contactModal;
		var buttonText = (this.state.formLoading) ? "Sending..." : "Send";

		var contactForm = (

			<form className="form-horizontal contact-form">

				<div className="form-group">
					<label htmlFor="contact-name" className="col-xs-2 control-label">Name</label>
					<div className="col-xs-10">
					<input type="text" id="contact-name" className="form-control" placeholder="Name" ref="name" />
					</div>
				</div>

				<div className="form-group">
					<label htmlFor="contact-email" className="col-xs-2 control-label">Email</label>
					<div className="col-xs-10">
					<input type="email" id="contact-email" className="form-control" placeholder="Email" ref="email" />
					</div>
				</div>

				<div className="form-group">
					<label htmlFor="contact-message" className="col-xs-2 control-label">Message</label>
					<div className="col-xs-10">
					<textarea id="contact-message" className="form-control" rows="4" placeholder="Message" ref="message"></textarea>
					</div>
				</div>

					<div className="form-group">
						<div className="col-xs-offset-2 col-xs-2">
							<button type="submit" className="btn btn-primary" onClick={this.sendMessage}>{buttonText}</button>
						</div>
						<div className="error-message col-xs-8">{this.state.errorMessage}</div>
					</div>
			
			</form>);
		
		var contactModal = (!this.state.sendStatus) ? 
			<Modal 	
				title={"Leave your message here"}
				bsStyle='default'
				backdrop={true}
				animation={true} 
				onRequestHide={this.props.closeHandler}>
				
				<div className="modal-body">
					{contactForm}
				</div>

			</Modal> :
			<Modal 	
				title={this.state.sendStatus}
				bsStyle='default'
				backdrop={true}
				animation={true} 
				onRequestHide={this.props.closeHandler}>
			</Modal>;

		return	contactModal;		
	}
});

module.exports = ContactForm;