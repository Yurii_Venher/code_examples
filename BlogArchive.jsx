'use strict';
var $ = require('jquery');
var _ = require('underscore');
var React = require('react');

var AdminMedia = require('./Admin/Media.jsx');

var BlogArchiveItem = React.createClass({

	clickHandler: function(event) {
		event.preventDefault();
		this.props.loadArticlesByDate(this.props.month+1, this.props.year);
	},

	render: function() {

		var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		
		return (
			<div className="blog-archive-item clearfix">
				<div className="row">
					<div className="blog-archive-item-title-section form-group col-xs-12">
						<span className="blog-archive-item-title" onClick={this.clickHandler}>
							{monthNames[this.props.month] + " " + this.props.year}
						</span>
					</div>
				</div>
			</div>
		);
	}
});

var BlogArchive = React.createClass({

	shouldComponentUpdate: function() {
		return false;
	},

	render: function() {

		var self = this;

		var date, month, year, archive, archiveItems, blogArchive;

		if (this.props.articles && this.props.articles.length > 0) {
			
			archive = this.props.articles.map(function(article) {
				date = article.date.split(" ")[0];
				month = date.split(".")[1] - 1;
				year = date.split(".")[2];
				return {month: month, year: year};
			});

			archive.sort(function(a,b) {
				return new Date(b.year, b.month).getTime() - new Date(a.year, a.month).getTime();
			});

			archiveItems = archive.map(function(item, index, array) {

				if (index === 0 || item.year !== array[index-1].year || item.month !== array[index-1].month) {

					return (<BlogArchiveItem 
								month={item.month} 
								year={item.year}
								loadArticlesByDate={self.props.loadArticlesByDate} />);
				}
			});

			blogArchive = 
				<div className="blog-archive-block">
					<h2 className="page-title">Archive</h2>
					<div className="list-group-item row">
						{archiveItems}
					</div>
				</div> ;
		}

		return (
			<div>
				{blogArchive}
			</div>
		);
	}
});

module.exports = BlogArchive;