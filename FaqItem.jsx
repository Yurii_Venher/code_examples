/**
 * @jsx React.DOM
 */

'use strict';
var $ = require('jquery'),
_ = require('underscore'),

React = require('react'),
Fluxxor = require("fluxxor"),
FluxMixin = Fluxxor.FluxMixin(React),
StoreWatchMixin = Fluxxor.StoreWatchMixin,

ContentItem = require('./../mixins/ContentItem.js'),
AddContentItem = require('./AddContentItem.jsx');


var WriteFaqItem = React.createClass({

	mixins: [ContentItem.WriteMode, FluxMixin, StoreWatchMixin("PageStore")],

	getStateFromFlux: function() {
	var PageStore = this.getFlux().store("PageStore");

		return {
			faqCategories: PageStore.faqCategories
		};
	},

	addItem: function (event) {
		event.preventDefault();

		this.props.onAdd({
			title: this.refs.title.getDOMNode().value,
			body: this.refs.body.getDOMNode().value,
			identifiedWith: this.refs.category.getDOMNode().value,
			position: 1
		});
		
		this.props.cancelHandler();
	},

	updateItem: function (event) {
		event.preventDefault();

		var obj = this.props.obj;
		obj.title = this.refs.title.getDOMNode().value;
		obj.body = this.refs.body.getDOMNode().value;
		obj.identifiedWith = this.refs.category.getDOMNode().value;
		this.props.onEdit(obj.id, obj);

		this.props.cancelHandler();
	},

	render: function() {
		var title, body, category;
		var buttons = this.renderButtons();

		var categories;
		if (this.state.faqCategories) {
			categories = this.state.faqCategories.map(function(faq) {
				return <option value={faq.id}>{faq.title}</option>
			});
		}

		if (!this.props.isAddMode) {
			title = this.props.obj.title;
			body = this.props.obj.body;
			category = this.props.obj.identifiedWith.id; 
		}

		return (
			<div className="faq-edit-item">
				<div className="list-group-item row">

					<div className="row">
						<div className="faq-item-title-section form-group col-xs-12">
							<input type="text" className="form-control" placeholder="Title" defaultValue={title} ref="title" />
						</div>
					</div>

					<div className="row">
						<div className="faq-item-body-section form-group col-xs-12">
							<textarea className="form-control" placeholder="Body" rows="5" defaultValue={body} ref="body"></textarea>
						</div>
					</div>

					<div className="row">
						<div className="faq-item-category-section form-group col-xs-12">
							<select className="form-control" defaultValue={category} ref="category">
								{categories}
							</select>
					  	</div>
					</div>

					<div className="row">
						<div className="edit-buttons col-xs-12">
							{buttons}
						</div>
					</div>

				</div>
			</div>
		);
	}
});


var ReadFaqItem = React.createClass({

	mixins: [ContentItem.ReadMode],

	render: function () {
		var adminButtons = this.renderButtons();

		return (
			<div className="faq-item">

				<div className="row">
					<h3 className="faq-item-title form-group col-xs-12">
						{this.props.obj.title}
					</h3>
				</div>

				<div className="row">
					<div className="faq-item-body form-group col-xs-12">
						{this.props.obj.body}
					</div>
				</div>
			
				{adminButtons}

			</div>
		);
	}
});


var FaqItem = React.createClass({

	mixins: [ContentItem.BaseContent],

	render: function () {
		var contentMode = this.renderMode(ReadFaqItem, WriteFaqItem, AddContentItem);

		return (
			<div>{contentMode}</div>
		);
	}
});


module.exports = FaqItem;