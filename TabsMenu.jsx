/**
 * @jsx React.DOM
 */

'use strict';

var $ = require('jquery');

var React = require('react'),
Modal = require('react-bootstrap').Modal,
TabbedArea  = require('react-bootstrap').TabbedArea,
TabPane  = require('react-bootstrap').TabPane,
Fluxxor = require("fluxxor"),
FluxMixin = Fluxxor.FluxMixin(React),
StoreWatchMixin = Fluxxor.StoreWatchMixin;

var ContactForm = require("./ContactForm.jsx");

var EditTab = React.createClass({
	
	removeItem: function (event) {
		event.preventDefault();

		this.props.onRemove(this.props.obj.id);		
	},

	addItem: function (event) {
		event.preventDefault();

		this.props.onAdd({
			title: this.refs.title.getDOMNode().value,
			body: this.refs.body.getDOMNode().value,
			document: 1,
			identifiedWith: "about/partners/contacts"
		});

		this.props.closeAddItem();	
	},

	updateItem: function (event) {
		event.preventDefault();

		var obj = this.props.obj;
		obj.title = this.refs.title.getDOMNode().value;
		obj.body = this.refs.body.getDOMNode().value;
		obj.document = null;

		this.props.onEdit(obj.id, obj, obj.identifiedWith);
	},

	render: function() {
		var title = (this.props.obj && this.props.obj.title) || null,
		body = (this.props.obj && this.props.obj.body) || null;
						
		var buttons = this.props.addMode ?
				(<div>
					<div className="btn-group"> 
					<button type="submit" className="btn btn-primary" onClick={this.addItem}>Add</button>
					<button type="submit" className="btn btn-default" onClick={this.props.closeAddItem}>Cancel</button> 
					</div>
				</div>):
				(<div>
					<div className="btn-group"> 
						<button type="submit" className="btn btn-primary" onClick={this.updateItem}>Save</button>
 				 		<button type="submit" className="btn btn-default" onClick={this.removeItem}>Remove</button>
					</div>
				</div>);

		return (
			<div className="list-group-item row">
				<div className="tab-title form-group col-xs-12">
					<input type="text" className="form-control" placeholder="Tab title" defaultValue={title} ref="title" />
				</div>
				<div className="tab-body form-group col-xs-12">
					<textarea className="form-control" rows="3" placeholder="Tab body" defaultValue={body} ref="body"></textarea>
				</div>
				<div className="form-group col-xs-6">
					<div className="btn-group"> 
						{buttons}
					</div>
				</div>
			</div>						
		);
	}
});

var NewTab = React.createClass({
			
	getInitialState: function() {
		return {
			inActive: true
		};
	},

	activateMode: function (event) {
		event.preventDefault();
		this.setState({
			inActive: this.state.inActive ? false : true
		});			
	},

	closeAddItem: function () {
		this.setState({
			inActive: this.state.inActive ? false : true
		});			
	},

	render: function(){
		var mode = this.state.inActive ?
			(<div className="form-group pull-left">
				<button type="submit" className="btn btn-primary" onClick={this.activateMode}>Add Tab</button>
			</div>) :
			<EditTab addMode={true} onAdd={this.props.onAdd} closeAddItem={this.closeAddItem} />;

		return (<div id="add-category">{mode}</div>)
	}
});


var WriteTabsMenu = React.createClass({

	activateReadMode: function () {
		this.props.cancelHandler();
	},

	componentDidMount: function () {
			$(".navbar-footer").addClass("staticFooter");
	},

	render: function () {
		var self = this;
		var list;
		var blockTitle = <h2 className="block-title col-xs-12">Basic Information Menu</h2>;
				
		if(this.props.items) {
			list = this.props.items.map(function(item) {
				return <EditTab obj={item} onEdit={self.props.onEdit} onRemove={self.props.onRemove} />;
			});
		}

		return (
			<div className="clearfix">
				<div className="tabs-list form-group clearfix">
					{blockTitle}
					{list}
					<NewTab onAdd={this.props.onAdd} />
					<button type="submit" className="btn btn-default" onClick={this.props.cancelHandler}>Back</button>
				</div>
			</div>);
	}
});

var ReadTabsMenu = React.createClass({

	getInitialState: function() {
		return {
			openOverlay: false
		};
	},

	openFeedbackModal: function () {
		this.setState({
			openOverlay: true
		});
	},

	handleOnOverlayClose: function () {
		this.setState({
			openOverlay: false
		});
		$(".section").removeClass("active");
	},

	render: function () {

		var tabsList, tabBody;

		var adminButtons = this.props.isAdmin ?
			(<div className="form-group clearfix">
					<button type="submit" className="btn btn-primary" onClick={this.props.editHandler}>Edit</button>
			</div>) :
			{};

		var feedback = 
			(<div>
				<div className="">Do you have some questions? Write to us!</div>
				<div className="form-group clearfix">
					<button type="submit" className="btn btn-success" onClick={this.openFeedbackModal}>Feedback</button>
				</div>
			</div>);

		var openOverlay = this.state.openOverlay ? <ContactForm closeHandler={this.handleOnOverlayClose}/> : {};

		if (this.props.items) {
			tabsList = this.props.items.map(function(item, index) {
				tabBody = (index === 0) ? 
					(<div className="row">
						<div className="tab-body-section col-xs-8">{item.body}</div>
						<div className="feedback-section col-xs-4">{feedback}</div>
						{openOverlay}
					</div>) : 
					(<div className="col-xs-12">{item.body}</div>);
				return (<TabPane eventKey={item.id} tab={item.title}>{tabBody}</TabPane>);
			});
		} else {
			return (<div className="clearfix"></div>);
		}

		var tabbedArea = (this.props.items.length > 0) ? 
			(<TabbedArea defaultActiveKey={this.props.items[0].id}>
					{tabsList}
				</TabbedArea>) : 
			<h2 className="block-title col-xs-12">Basic Information Menu</h2>;

		return (
			<div className="clearfix">
				
				{tabbedArea}

				{adminButtons}	

			</div>			
		);
	}
});

var TabsMenu = React.createClass({

	getInitialState: function() {
		return {
			isReadMode: true
		};
	},

	activateReadMode: function () {
		this.setState({
			isReadMode: true
		});
	},
	
	editElement: function () {
		this.setState({
			isReadMode: false
		});
	},

	render: function () {
		
		var tabs = this.props.items;
		var mode = (this.props.isAdmin && !this.state.isReadMode) ?

			<WriteTabsMenu         
				cancelHandler={this.activateReadMode}
				onEdit={this.props.onEdit}
				onAdd={this.props.onAdd}
				onRemove={this.props.onRemove}
				items={tabs} /> :

			<ReadTabsMenu 
				isAdmin={this.props.isAdmin} 
				editHandler={this.editElement}
				items={tabs} />;

		return (<div>{mode}</div>);
	}
});

module.exports = TabsMenu;