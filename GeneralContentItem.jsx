/**
 * @jsx React.DOM
 */

'use strict';
var $ = require('jquery'),
_ = require('underscore'),

React = require('react'),
Fluxxor = require("fluxxor"),
FluxMixin = Fluxxor.FluxMixin(React),
StoreWatchMixin = Fluxxor.StoreWatchMixin,
Router = require('react-router'),
Navigation = Router.Navigation,
Link = Router.Link,

ContentItem = require('./../mixins/ContentItem.js'),

AdminMedia = require('./Admin/Media.jsx'),
AddContentItem = require('./AddContentItem.jsx'),
Modal = require('./Modal.jsx');


var WriteGeneralItem = React.createClass({

	mixins: [ContentItem.WriteMode, FluxMixin, StoreWatchMixin("PageStore")],

	getStateFromFlux: function() {
		var PageStore = this.getFlux().store("PageStore");

		return {
			contentCategories: PageStore.contentCategories
		};
	},

	getDefaultProps: function () {
		return {
				basePath: '/uploads/',
			}
	},

	addItem: function (event) {
		event.preventDefault();

		this.props.onAdd({
			title: this.refs.title.getDOMNode().value,
			body: this.refs.body.getDOMNode().value,
			document: this.state.imageId || null,
			identifiedWith: this.refs.category.getDOMNode().value,
		});
		
		this.props.cancelHandler();
	},

	updateItem: function (event) {
		event.preventDefault();

		var obj = this.props.obj;
		obj.title = this.refs.title.getDOMNode().value;
		obj.body = this.refs.body.getDOMNode().value;
		obj.document = this.state.imageId || obj.document.id;
		obj.identifiedWith = this.refs.category.getDOMNode().value;
		this.props.onEdit(obj.id, obj);

		this.props.cancelHandler();
	},

	renderThumb: function(document) {
		var basePath = "/uploads/";

		return (this.state.imageId || (document && document.category && document.documentName)) ?
			<img src={basePath + (this.state.categoryName || document.category) + "/" + (this.state.documentName || document.documentName)} width="100%"/> :
			null;
	},

	render: function() {
		var title, body, category;
		var openOverlay = this.renderOverlay(Modal, AdminMedia);
		var buttons = this.renderButtons();
		var image = this.renderThumb((this.props.obj && this.props.obj.document) || null);

		var categories;
		if (this.state.contentCategories) {
			categories = this.state.contentCategories.map(function(category) {
				return <option value={category.id}>{category.title}</option>
			});
		}

		if (!this.props.isAddMode) {
			title = this.props.obj.title;
			body = this.props.obj.body;
			category = this.props.obj.identifiedWith.id; 
		}

		return (
			<div className="general-edit-item">
				<div className="list-group-item row">

					<div className="general-item-thumb col-xs-4">
						{image} 
					</div>

					<div className="general-item-content col-xs-8">
						<div className="row">
							<div className="general-item-title-section form-group col-xs-12">
								<input type="text" className="form-control" placeholder="Title" defaultValue={title} ref="title" />
							</div>
						</div>

						<div className="row">
							<div className="general-item-body-section form-group col-xs-12">
								<textarea className="form-control" placeholder="Body" rows="5" defaultValue={body} ref="body"></textarea>
							</div>
						</div>

						<div className="row">
							<div className="general-item-category-section form-group col-xs-12">
								<select className="form-control" defaultValue={category} ref="category">
									{categories}
								</select>
							</div>
						</div>

						<div className="row>">
							<div className="change-image-button form-group col-sm-12"> 
								<button type="submit" className="btn btn-default" onClick={this.openMedia}>Change image</button>  
									{openOverlay}
							</div>
						</div>

						<div className="row">
							<div className="edit-buttons col-xs-12">
								{buttons}
							</div>
						</div>

					</div>
				</div>
			</div>
		);
	}
});


var ReadGeneralItem = React.createClass({

	mixins: [Navigation, ContentItem.ReadMode, FluxMixin, StoreWatchMixin("PageStore")],

	getStateFromFlux: function() {
		return {};
	},
	
	linkTo: function () {
		if (this.props.obj.routing) {
			this.replaceWith("/ukr-muc/" + this.props.obj.identifiedWith.routing + "/" + this.props.obj.routing + "-" + this.props.obj.id);
			if (this.props.clickHandler) this.props.clickHandler("ukr_mun", this.props.obj.id);
		}
	},

	renderThumb: function(document) {
		var basePath = "/uploads/";

		return (document && document.category && document.documentName) ?
			<img src={basePath + document.category + "/" + document.documentName} alt={document.documentName} width="100%" /> :
			null;
	},

	render: function () {
		var adminButtons = this.renderButtons(),
				thumb = this.renderThumb(this.props.obj.document);

		return (
			<div className="general-item">
		
				<div className="row">
					<div className="link-area" onClick={this.linkTo}>
						<div className="general-item-thumb form-group col-xs-4">
							{thumb}
						</div>
						<div className="general-item-content form-group col-xs-8">
							<div className="row">
								<h3 className="general-item-title form-group col-xs-12">
									{this.props.obj.title}
								</h3>
							</div>
							<div className="row">
								<div className="general-item-body form-group col-xs-12">
									{this.props.obj.body}
								</div>
							</div>
						</div> 
					</div>
				</div>

				{adminButtons}

			</div>
		);
	}
});


var GeneralContentItem = React.createClass({

	mixins: [ContentItem.BaseContent],

	render: function () {
		var contentMode = this.renderMode(ReadGeneralItem, WriteGeneralItem, AddContentItem);

		return (
			<div>{contentMode}</div>
		);
	}
});


module.exports = GeneralContentItem;